package com.example.t3_n00028416;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.squareup.picasso.Picasso;

import java.util.List;

public class apadterOke extends RecyclerView.Adapter<apadterOke.AdapterCotacViewHolder> {


    List<poke> list;
    Context mContext;


    public apadterOke(List<poke> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public AdapterCotacViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdapterCotacViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_poke, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCotacViewHolder holder, int position) {

        holder.SetInfo(list.get(position));

        poke aClass = list.get(position);
        Log.e("ulr", aClass.getUrl_imagen());

        holder.poke_ca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, detalleActivity.class);
                intent.putExtra("Poke", aClass);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class AdapterCotacViewHolder extends RecyclerView.ViewHolder {

        ImageView imageURl;
        TextView name;
        Button poke_ca;


        public AdapterCotacViewHolder(@NonNull View itemView) {
            super(itemView);
            imageURl = itemView.findViewById(R.id.imageURL);
            name = itemView.findViewById(R.id.name);
            poke_ca = itemView.findViewById(R.id.Verpokemon);
        }

        void SetInfo(poke aClass) {




            Picasso.get()
                    .load(aClass.getUrl_imagen())
                    .resize(50, 50)
                    .centerCrop()
                    .into(imageURl);
            name.setText(aClass.getNombre());
        }
    }
}
