package com.example.t3_n00028416;

import java.io.Serializable;

public class poke implements Serializable {

    String nombre, tipo, url_imagen;

    Double latitud, longitude;

    public poke(String nombre, String tipo, String url_imagen, Double latitud, Double longitude) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.url_imagen = url_imagen;
        this.latitud = latitud;
        this.longitude = longitude;
    }

    public poke() {
    }


    public poke(String nombre, String tipo, String url_imagen) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.url_imagen = url_imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public String getUrl_imagen() {
        return url_imagen;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setUrl_imagen(String url_imagen) {
        this.url_imagen = url_imagen;
    }

}

