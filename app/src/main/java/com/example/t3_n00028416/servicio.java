package com.example.t3_n00028416;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface servicio {

    @GET("N000284161")
    Call<List<poke>> getAll();

    @POST("N000284161/crear")
    Call<Void> POST(@Body poke pok);


}
