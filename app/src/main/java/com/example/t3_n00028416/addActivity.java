package com.example.t3_n00028416;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class addActivity extends AppCompatActivity {

    EditText nombre, tipo, url_imagen, latitude, longitude;
    Button registrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        nombre = findViewById(R.id.nombre);
        tipo = findViewById(R.id.tipo);
        url_imagen = findViewById(R.id.image);
        latitude = findViewById(R.id.latitude);
        longitude = findViewById(R.id.longitude);
        registrar = findViewById(R.id.registrar);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://upn.lumenes.tk/pokemons/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        servicio service = retrofit.create(servicio.class);

        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String no = nombre.getEditableText().toString();
                String ti = tipo.getEditableText().toString();
                String url = url_imagen.getEditableText().toString();
                String lat = latitude.getEditableText().toString();
                String lon = longitude.getEditableText().toString();
                poke model = new poke(no, ti, url, Double.parseDouble(lat), Double.parseDouble(lon));

                postP(model, service);
            }
        });

    }

    private void postP(poke model, servicio service) {

        Log.e("ulr", service.toString());

        Call<Void> pokeCall = service.POST(model);

        Log.e("call", pokeCall.toString());
        pokeCall.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.e("MAIN_APP", String.valueOf(response.code()));

                String code = String.valueOf(response.code());
                if (code.equals("200")) {
                    Toast.makeText(getApplicationContext(), "Guardado", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                } else {
                    Toast.makeText(getApplicationContext(), "NO Publicado", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }

        });
    }
}